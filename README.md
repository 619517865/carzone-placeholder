#carzone-placeholder<br/>
（1）、继承AbstractPropertiesTemplate实现子类<br/>
（2）、在placeholder.properties新建类似下面结构的<br/>
   dubbo为spring配置文件里面的id或者name,com.carzone.placeholder.template.DubboPropertiesTemplate类名，必须继承AbstractPropertiesTemplate，重新对${dubbo.protocol.host}重新自定义
   dubbo=com.carzone.placeholder.template.DubboPropertiesTemplate<br/>
   
 使用：<br/>
 	 &lt;bean id="zookeeperPropertyLoad"<br/>
		class="com.carzone.placeholder.CustomPropertyPlaceholderConfigurer"><br/>
		&lt;property name="order" value="1" /><br/>
		&lt;property name="ignoreResourceNotFound" value="true"/><br/>
		&lt;property name="ignoreUnresolvablePlaceholders" value="true" /><br/>
		&lt;property name="location"><br/>
			&lt;value>file:d:\\ip1&lt;/value><br/>
		&lt;/property><br/>
	&lt;/bean>
 <br/>
例如对：host重定义，如果host存在，则用dubbo.protocol.host22222替换的值，否则自定义默认值<br/>
&lt;dubbo:protocol name="dubbo" server="netty"<br/>
		serialization="hessian2" threadpool="fixed" threads="1200"<br/>
		host="${dubbo.protocol.host22222}" port="${dubbo.protocol.port2}" /><br/>
虽然代码很Low，但是功能基本实现