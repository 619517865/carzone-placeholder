package com.carzone.placeholder;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import com.carzone.placeholder.factory.PropertiesTemplateFactory;
import com.carzone.placeholder.template.AbstractPropertiesTemplate;
import com.carzone.placeholder.util.PropertyValuesVisitorUtil;

public class CustomizationResourcePlaceholderConfigurer extends CustomizationResourcePlaceholderSupport
		implements BeanNameAware, BeanFactoryAware {

	private String beanName;

	private BeanFactory beanFactory;

	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;

	}

	@Override
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	@Override
	protected void doPostProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
		String[] beanNames = beanFactory.getBeanDefinitionNames();
		AbstractPropertiesTemplate propertiesTemplate = null;
		for (String curName : beanNames) {
			if (!(curName.equals(this.beanName) && beanFactory.equals(this.beanFactory))) {
				propertiesTemplate = PropertiesTemplateFactory.doCreateNewPropertiesTemplate(curName);
				// 如果是true才去处理，否则不处理
				if (propertiesTemplate.isHandle(curName)) {
					BeanDefinition bd = beanFactory.getBeanDefinition(curName);
					PropertyValuesVisitorUtil.resolveProperties(bd.getPropertyValues(), propertiesTemplate);
				}
			}
		}
	}
}
