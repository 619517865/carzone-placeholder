package com.carzone.placeholder;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

public abstract class CustomizationResourcePlaceholderSupport implements BeanFactoryPostProcessor, PriorityOrdered{

	private int order = Ordered.LOWEST_PRECEDENCE;  // default: same as non-Ordered
	
	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return this.order;
	}
	
	/**
	 * <p>Title: postProcessBeanFactory</p>
	 * <p>Description: </p>
	 * @author xfyang
	 * @date 2017年4月21日 下午2:04:51 
	 * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
	 */
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		doPostProcessBeanFactory(beanFactory);
	}

	/**
	 * @Title: doPostProcessBeanFactory 
	 * @Description: 子类继承实现
	 * @param beanFactory
	 * @return void
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月21日 下午2:04:56
	 */
	protected abstract void doPostProcessBeanFactory(ConfigurableListableBeanFactory beanFactory);
}
