package com.carzone.placeholder.enums;

/**
 * @ClassName: SymbolEnums
 * @Description:占位符枚举
 * @version V1.0
 * @author: xfyang
 * @date: 2017年4月14日 下午12:53:39
 */
public enum SymbolEnums {

	DOLLAR_LEFT("${","左大括号占位符"),
	DOLLAR_RIGHT("}","右占位符");
	
	private String type;
	private String desc;
	
	private SymbolEnums(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}
	public String getType() {
		return type;
	}
	public String getDesc() {
		return desc;
	}
}
