package com.carzone.placeholder.factory;

import java.util.Map;

import com.carzone.placeholder.strategy.PropertiesTemplateStragety;
import com.carzone.placeholder.template.AbstractPropertiesTemplate;
import com.carzone.placeholder.template.DefaultPropertiesTemplate;

/**
 * @ClassName: PropertiesTemplateFactory
 * @Description:根据传进来的name,创建对应的对象(工厂方法)
 * @version V1.0
 * @author: xfyang
 * @date: 2017年4月21日 下午1:52:48
 */
public class PropertiesTemplateFactory {

	/**
	 * @Title: doCreateNewPropertiesTemplate
	 * @Description: 根据name获取对应的处理器
	 * @param name beanName
	 * @return AbstractPropertiesTemplate
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 下午12:38:32
	 */
	public static AbstractPropertiesTemplate doCreateNewPropertiesTemplate(String name) {
		Map<String, AbstractPropertiesTemplate> map = PropertiesTemplateStragety.getMap();
		AbstractPropertiesTemplate abstractPropertiesTemplate = map.get(name);
		if (abstractPropertiesTemplate == null) {
			return new DefaultPropertiesTemplate();
		}
		return abstractPropertiesTemplate;
	}
}
