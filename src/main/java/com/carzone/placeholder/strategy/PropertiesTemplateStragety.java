package com.carzone.placeholder.strategy;

import java.util.Map;

import com.carzone.placeholder.template.AbstractPropertiesTemplate;
import com.carzone.placeholder.util.PlaceholderUtil;
import com.google.common.collect.Maps;

/**
 * @ClassName: PropertiesTemplateStragety
 * @Description: 策略模式加载所有继承与AbstractPropertiesTemplate模板
 * 在placeholder.properties里面配置
 * @version V1.0
 * @author: xfyang
 * @date: 2017年4月14日 下午2:23:04
 */
public class PropertiesTemplateStragety {

	private static Map<String, AbstractPropertiesTemplate> map = Maps.newConcurrentMap();
	
	static {
		map = PlaceholderUtil.readProperties();
	}

	public static Map<String, AbstractPropertiesTemplate> getMap() {
		return map;
	}
}