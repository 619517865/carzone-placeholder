package com.carzone.placeholder.template;

import org.springframework.beans.PropertyValue;

/**
 * @ClassName: AbstractPropertiesTemplate
 * @Description: 属性模板处理
 * @version V1.0
 * @author: xfyang
 * @date: 2017年4月14日 上午10:36:22
 */
public abstract class AbstractPropertiesTemplate {

	/**
	 * @Title: isHandle 
	 * @Description: 是否需要处理没有被映射的${},默认不处理
	 * @param name
	 * @return boolean
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 下午12:45:05
	 */
	public boolean isHandle(String name) {
		return false;
	}
	
	/**
	 * @Title: doProcess 
	 * @Description: 处理不同的
	 * @param valueResolver
	 * @param value
	 * @return Object
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 上午10:36:25
	 */
	public abstract Object doProcess(PropertyValue pv);
}