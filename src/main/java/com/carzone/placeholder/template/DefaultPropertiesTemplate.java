package com.carzone.placeholder.template;

import org.springframework.beans.PropertyValue;

public class DefaultPropertiesTemplate extends AbstractPropertiesTemplate{

	@Override
	public Object doProcess(PropertyValue pv) {
		return pv.getValue();
	}

}
