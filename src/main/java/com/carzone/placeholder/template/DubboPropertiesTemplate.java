package com.carzone.placeholder.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyValue;

import com.carzone.placeholder.enums.SymbolEnums;
import com.carzone.placeholder.util.NetworkUtils;

/**
 * @ClassName: DubboPropertiesTemplate
 * @Description: dubbo属性处理
 * @version V1.0
 * @author: xfyang
 * @date: 2017年4月14日 下午12:49:43
 */
public class DubboPropertiesTemplate extends AbstractPropertiesTemplate {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static final String HOST = "host";

	private static final String PORT = "port";

	@SuppressWarnings("unused")
	private static final String DEFAULT_PORT = "-1";

	/**
	 * @Title: isHandle
	 * @Description: 是否需要处理没有被映射的${}
	 * @param name
	 * @return boolean
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 下午12:45:05
	 */
	@Override
	public boolean isHandle(String name) {
		return true;
	}

	/**
	 * @Title: doProcess
	 * @Description: 处理dubbo,入库没有配置主机名则自动获取
	 * @param valueResolver
	 * @param pv
	 * @return Object
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 下午12:49:26
	 */
	@Override
	public Object doProcess(PropertyValue pv) {
		String name = pv.getName();
		Object valueObj = pv.getValue();
		if (valueObj instanceof String) {
			String value = (String) valueObj;
			// 如果是host没有对应的${,则自动获取当前的主机IP
			if (name.trim().equals(HOST)) {
				// HOST包含${
				if (value.contains(SymbolEnums.DOLLAR_LEFT.getType())) {
					value = NetworkUtils.getHostName();
					logger.info("dubbo自动获取的主机{}={}", valueObj, value);
				}

			} else if (name.trim().endsWith(PORT)) {
				// PORT包含${
				/*if (value.contains(SymbolEnums.DOLLAR_LEFT.getType())) {
					value = DEFAULT_PORT;
					logger.info("使用默认的端口{}={}", valueObj, value);
				}*/
			}
			return value;
		}
		return valueObj;
	}

}
