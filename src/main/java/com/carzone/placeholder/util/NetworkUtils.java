package com.carzone.placeholder.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * @ClassName: NetworkUtil
 * @Description: 获取客户端网络信息工具类
 * @version V1.0
 * @author: chuang
 * @date: 2016年6月2日 下午2:40:02
 */
public final class NetworkUtils {

	private static final Logger logger = LoggerFactory.getLogger(NetworkUtils.class);

	/**
	 * @Title: getIpAddress
	 * @Description: 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址;
	 * @param request
	 * @throws IOException
	 * @return String
	 * @version V1.0
	 * @author chuang
	 * @Date 2016年6月2日 下午2:42:13
	 */
	public static String getRequestSourceIp() {
		try {
			String ipAddress = null;

			if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
				// 根据网卡取本机配置的IP
				ipAddress = InetAddress.getLocalHost().getHostAddress();
			}

			// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
			if (ipAddress != null && ipAddress.length() > 15) {
				if (ipAddress.indexOf(",") > 0) {
					ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
				}
			}
			// 或者这样也行,对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
			// return
			// ipAddress!=null&&!"".equals(ipAddress)?ipAddress.split(",")[0]:null;
			return ipAddress;
		} catch (Exception e) {
			logger.error("get romote ip error,error message:" + e.getMessage());
			return "";
		}
	}

	public static InetAddress getInetAddress() {

		try {
			return InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			logger.error("获取InetAddress失败",e);
		}
		return null;

	}

	/**
	 * @Title: getHostIp 
	 * @Description: 获取主机ip
	 * @param netAddress
	 * @return String
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月17日 上午9:17:44
	 */
	public static String getHostIp(InetAddress netAddress) {
		if (null == netAddress) {
			return null;
		}
		return netAddress.getHostAddress(); // get the ip address
	}

	/**
	 * @Title: getHostName 
	 * @Description: 获取主机name
	 * @param netAddress
	 * @return String
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月17日 上午9:17:57
	 */
	public static String getHostName(InetAddress netAddress) {
		if (null == netAddress) {
			return null;
		}
		return netAddress.getHostName(); // get the host address
	}

	/**
	 * @Title: getHostName 
	 * @Description: 先获取主机名称、没有这获取主机IP
	 * @return String
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月17日 上午9:18:15
	 */
	public static String getHostName() {
		InetAddress inetAddress = getInetAddress();
		String hostName = null;
		if (inetAddress != null) {
			hostName = inetAddress.getHostName();
		}
		if (Strings.isNullOrEmpty(hostName)) {
			hostName = inetAddress.getHostAddress();
		}
		return hostName;
	}
}