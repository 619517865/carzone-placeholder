package com.carzone.placeholder.util;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.util.ObjectUtils;

import com.carzone.placeholder.template.AbstractPropertiesTemplate;

/**
 * @ClassName: PropertyValuesVisitorUtil
 * @Description: 针对${xxxx}做特殊处理
 * @version V1.0
 * @author: xfyang
 * @date: 2017年4月14日 上午10:29:13
 */
public class PropertyValuesVisitorUtil {

	/**
	 * @Title: resolveProperties 
	 * @Description: 
	 * @param pvs
	 * @param valueResolver
	 * @return void
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 上午10:30:14
	 */
	public static void resolveProperties(MutablePropertyValues pvs,AbstractPropertiesTemplate propertiesTemplate) {
		PropertyValue[] pvArray = pvs.getPropertyValues();
		for (PropertyValue pv : pvArray) {
			Object newVal = resolveValue(propertiesTemplate,pv);
			if (!ObjectUtils.nullSafeEquals(newVal, pv.getValue())) {
				pvs.add(pv.getName(), newVal);
			}
		}
	}

	/**
	 * @Title: resolveValue 
	 * @Description: 
	 * @param propertiesTemplate
	 * @param valueResolver
	 * @param pv
	 * @return Object
	 * @version V1.0
	 * @author xfyang
	 * @Date 2017年4月14日 上午10:37:36
	 */
	private static Object resolveValue(AbstractPropertiesTemplate propertiesTemplate,PropertyValue pv) {
		return propertiesTemplate.doProcess(pv);
	}
}
